<?php
	//Dateizugriff
		$url = 'data.json';
		$data = file_get_contents($url);

	//JSON-Funktionen
		//Decoding
			$data = '{
				"name": "Mike",
				"job": "Writer"
			}';

			$result = json_decode($data,true);  	// Assoziatives Array		$result['name']
			$result = json_decode($data,false); 	// -> Objekt				$result->name
			$result = json_decode($data);	 		// like false				$result->name
	
		//Encoding Array
			$data = [
				'name' => 'Mike',
				'job' => 'Writer',
				'salary' => 500
			];
		 
			json_encode($data); // Result = JsonString
			
		//Encoding Objekt
			class Person {
				public $name;
				public $job;
				public $salary;
			}
			
			$person = new Person();
			$person->name = "Mike";
			$person->job = "Writer";
			$person->salary = 500;
			
			json_encode($person); // Result = JsonString
	
	//Basics
		//For Schleife
			$sum = 0
			for($i=0; $i<100; $i++) {              
				if($i % 3 == 0) {
					$sum += $i;
				}
			}
		
		//Foreach Schleife
			foreach($array as $element) {
				echo "<tr><td>Element is $element</td></tr>";
			}
		
		//Arrays
		    $arr1 = array('first', 'sedond', 'third'); 
			$arr1[] = "fourth";			 
			array_push($arr1, "fifth");
		
		//Assoziative Arrays
			$car_parts = ['tires' => 12, 
                    'brakes' => 8, 
                    'seats' => 40, 
                    'steering_wheels' => 5];
                     
			echo $car_parts['steering_wheels'];
 
			foreach($car_parts as $key=>$value) {
				echo "$key --- $value <br>";
			}
		//GLOBAL
			$test = "hallo"
			
			function bla(){
				global $test;
				echo $test // -> "hallo"
			}
			
		//SWITCH
			switch($variable) {
				case 'GET':
					//....
					break;
				
				case 'POST':
					//....
					break;
					
				case 'PUT':
					//....
					break;
				
				case 'DELETE':
					//....
					break;
				
				default:
					//....
					break;
			}
		
		//intval
			echo intval(42);                      // 42
			echo intval(4.2);                     // 4
			echo intval('42');                    // 42
			echo intval('+42');                   // 42
			echo intval('-42');                   // -42
			
	
	//Sessionhandling
		//Session Start
			session_start();
		
			//Zuweiseung
			$_SESSION['test'] = 25;
			 
			//Prüfen ob Sessionvariable gesetzt + Zuweisung zu lokaler Variable
			if(isset($_SESSION['name'])) {
				$name=$_SESSION['name'];
			} else {
				$name = "";
			}
		
		//Session Stop
		    session_start();
			session_destroy();
			
	//Formhandling
		//In html
		//<form method="GET" action="bla.php">
		//<input type="text" name="id"/>
		//<input type="text" name="name"/>
		//<input type="submit">Absenden</input>
		//</form>
		if(isset($_GET['id'])
			$id = $_GET['id'];
		
		$name = $_GET['name'];
		
	//Datenbankzugriff
		//Select
			$db = new mysqli('localhost', 'root', '', 'zoo');
			 
			if(mysqli_connect_errno()) {
				echo "Problem connecting DB. Check your settings <br>";
				exit;   
			}
		 
			$query = "select * from animals where numberOfLegs=?;";
			 
			// Prepared statement
			$pstmt = $db->prepare($query);
			$pstmt->bind_param('i', $numberOfLegs);
			$pstmt->execute();
			 
			$result = $pstmt->get_result();
			$row = $result->fetch_assoc();
		

		//Delete
			$id = $_GET['id'];
			$db = new mysqli('localhost', 'root', '', 'zoo');
			 
			if(mysqli_connect_errno()) {
				echo "Problem connecting DB. Check your settings <br>";
				exit;   
			}
			 
			$query = "delete from animals where numberOfLegs=?;";
			 
			// Prepared statement
			$pstmt = $db->prepare($query);
			$pstmt->bind_param('i', $id);
			$pstmt->execute();
	
		//Sonstiges
			$query = "SELECT * FROM employee";
			$response = array();
			$result = mysqli_query($connection, $query);
			
			while($user=mysqli_fetch_assoc($result)) {
				$response[] = $user;
			}
			header("Content-Type: application/json");
		
	//Diverses Auslesen des Input-Bodys	
		file_get_contents('php://input')
		
	//Diverses Handling von Response
		$response = array(
			'status' => 1,
			'status_message' => 'Employee added successfully'
		);
?>

<!--JAVSSCRIPT-->
<script>
	var xmlhttp = new XMLHttpRequest();
	//oder onload = ....
	xmlhttp.onreadystatechange = function() {
		/*
			readyState
			0: request is not initialized
			1: server connection is established
			2: request is received
			3: processsing request
			4: request finished and the response is ready
		 
			status
			200 - ok
			404 - page not found
		*/
		//Wartet bis server.php erfolgreich geladen wurde und schreibt anschließend in das Element 'ElementAufWebseite'
		//Den Namen und den Job 
		if(this.readyState==4 && this.status==200) {
			//ResponseText (Json) in Objekt umwandeln
			myObject = JSON.parse(this.responseText);
			document.getElementById('ElementAufWebseite').innerHTML = myObject.name;                  
			document.getElementById('ElementAufWebseite').innerHTML += myObject.job; 
		}
	};                  
	 
	//Methode, URL, isAsync
	xmlhttp.open("GET", "server.php", true);
	xmlhttp.send();
<script>